### Mise en place
Le script s'appuit sur le fichier [.backupconf](./dotfiles/.backupconfig) ce fichier doit se trouver dans votre $HOME pour que le script fonctionne

### Configuration
Vous pouvez configurer le fonctionnement du script en modifiant .backupconfig

**backupPath** : Chemin vers le dossier de sauvegarde.  
**maxSave** : Nombre de sauvegarde que vous souhaitez conserver, le script supprimera les plus anciennes si nécessaire.  
**user** : Permet d'utiliser un user mysql différent du user executant le script.  
**secret** : Permet d'entrer le mot de passe du user.  
**database** : Permet des lister les bases de donner que vous souhaitez sauvegarder. Laisser "" pour que la sauvegarde conserve la totalité des bases de données.  
**logfile** : Chemin vers le fichier de log du script.  

### Lancer une Backup
Pour lancer une sauvegarde il suffit de d'executer le script [savebdd.sh](./scripts/savebdd.sh).

### Réintégrer une Backup
Pour restaurer une sauvegarde il suffit d'entrer la commande suivante dans un prompt MySQL :
`source \chemin\du\Backup.sql;`
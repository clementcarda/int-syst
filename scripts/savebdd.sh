#! /bin/sh


## récupère le fichier backupconf
source $HOME/.backupconf

#exec $(date '+%Y-%m-%d_%H-%M-%S') || 2>> $logfile
exec >> $logfile 2>&1

## initialisation des variables
savedate=$(date '+%Y-%m-%d_%H-%M-%S')
nbSave=$(ls $backupPath/ | wc -l)

if [[ -z $user ]]; then
	user=$(echo $USER)
fi

## supression des saves excedantaire
if [[ $nbSave -ge $maxSave ]]; then
	nbDel=$(expr $nbSave - $maxSave + 1)
	listDel=$(ls $backupPath/ | sort | head -$nbDel)
	for save in $listDel
	do
		rm $backupPath/$save
	done
fi

## lancement du backup
if [[ $databases != "" ]]
then
	mysqldump -u $user -p$secret --dump-date --databases $databases > $backupPath/backupbdd_$savedate.sql
else
	mysqldump -u $user -p$secret --all-databases --dump-date > $backupPath/backupbdd_$savedate.sql
fi

## récupération du code erreur
sqlerr=$?

if [[ $sqlerr -eq 0 ]]
then
	echo "everything is fine" >> $logfile
	exit 0
else
	echo "message d'erreur" >> $logfile
	exit 1
fi

#! /bin/bash

source $HOME/.backansconf

## sauvegarde des bdd
for rms in $remoteserv; do
	rsync -r $user@$rms:$bddpath ~/Backup
done

## verification de la procédure
result=$?
if [[ $result -ne 0 ]]; then
	echo "message d'erreur" >> $logfile
	exit 1
fi


## sauvegarde des fichiers de conf ansible
for ans in $ansibleserv; do
	rsync -r $user@$ans:$confpath ~/AnsBackup
done

## verification de la procédure
result=$?
if [[ $result -eq 0 ]]; then
	echo "everything is fine" >> $logfile
	exit 0
else
	echo "message d'erreur" >> $logfile
	exit 1
fi
#Index

## Laptop
l'ensemble des informations sur le fonctionnement et les solution pour fiabiliser mon [laptop](./laptop.md)

## Projet Integration
Décrit le projet d'[intégration](./projet-integration.md)

## Script sauvegarde
Le script de sauvegarde et son fonctionnement sont décrit dans le fichier de [scripting](./scripting.md)
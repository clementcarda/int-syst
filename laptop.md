# Gestion des mots de passes
Tous mes mots de passes sont sur une bases de donnée Keepass.

# Gestion des sauvegardes
Tous les cours sont dans un dossier qui se synchronise automatiquement avec mon OneDrive Ynov.
Tous les soirs je sauvegarde sur un disque dur externe avec une commande `rsync`
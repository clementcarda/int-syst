# Projet d'intégration

## Introduction
Mise en place d'un playbook Ansible qui pourra être lancé via un Vagrantfile.
Une installation Ansible pouvant pousser automatiquement un wiki accessible en ligne.
Dans le cadre du labo la structure fonctionne avec des VM Virtualbox monter grâce à un fichier Vagrantfile.

## Infrastructure
Notre projet fonctionnera sur 3 machines avec 2 réseaux, l'un d'eux simulant l’extérieur.

![schéma de l'infra](./images/TP_FilRouge.png)

## Installation d'Ansible
Ansible peut facilement s'installer via le package manager sur Red Hat, CentOS, Fedora, Debian ou Ubuntu.
Afin d'avoir la dernière version d'Ansible sur debian il convient d'ajouter le dépôt Ansible, pour cela dans `/etc/apt/sources.list.d/ansible.list`
```
0a1
> deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
```
Enfin pour installer le paquet il suffit d'update apt-get et d'installer Ansible.

```bash
sudo apt-get update
sudo apt-get install ansible
ansible --version
```
Le fichier [ansible.cfg](./ansible.cfg) peut servir à surcharger la configuration de base d'Ansible.
On peut notamment s'en servir pour choisir notre répertoire d'installation pour les rôles avec Ansible Galaxie.
Pour cela il suffit de copier le fichier dans notre $HOME.

## Installation de Vagrant
Vagrant est outil permettant de lancer et de configurer des VM. Sont installation se fait très simplement via le gestionnaire de paquet.
```bash
sudo apt-get install vagrant
vagrant --version
```
## Playbook
Le playbook est un ensemble de rôles et d'instruction qu'Ansible va accomplir. Ici il nous sert de dossier courant pour notre projet.
Il contient le Vagrantfile, les playbook ainsible ainsi que les rôles que nous allons utiliser.

### Les Rôles
L'installation de notre infrastructure se fait essentiellement grâce à 4 rôles.

#### Mariadb
Notre serveur de base de donnée est configurer grâce au rôle clement.mariadb.
Ce denier installe les paquets nécessaire, créer notre base de donnée pour le wiki ainsi que les tables nécessaire. Enfin il créé le user wikimedia avec tous les droits nécessaire pour le bon fonctionnement de notre projet.

#### Wikimedia
Notre serveur Wikimedia est provisionné par 3 rôles distincts.
Les rôles geerlingguy.apache et geerlingguy.php permettent d'installer tous les services pour pouvoir faire tourner Wikimedia.
Le rôle clement.wikimedia déploie le wiki avec le fichier [LocalSettings.php](./roles/files/LocalSettings.php) contenant toute la configuration pour que le Wiki soit opérationnel.

### Molecule
Molecule est une application permettant de tester nos rôles en les exécutants dans un conteneur.
On peut voir facilement la configuration dans les fichiers .yml se situant dans les dossiers molecules de nos rôles.

## Installation de la Solution
Grâce aux miracles de l'automatisation l'installation de nos serveur de base de données et de notre Wikimedia ne nécessite qu'une ligne de commande à entrer dans le dossier courant du Playbook.
`vagrant up`
La création des VM et leurs provisionning via Ansible se fera automatiquement par [Vagrant](./playbook/Vagrantfile).

## Sauvegarde et restauration de la solution
En raison de la facilité de déploiement de la solution et de sa légèreté, la présence du playbook dans le gitlab est suffisant. En revanche la Base de Données constitué par notre Wiki est critique à conserver. Il serait dommage de devoir refaire tous le wiki chaque fois qu'on le redéploie.
Pour sauvegarder la BDD on fait appelle à un script [mysqldump](./scriping.md) qui créera un dump .sql de la base avec toutes nos tables. Ce fichier peut ensuite être conservé dans un serveur de backup grace à un script [rsync](./scripting.md).
Pour restaurer la solution il suffit de remplacer le [fichier sql](./playbook/roles/clement.mariadb/files/wikibdd.sql) en s'assurrant d'avoir garder le nom wikibdd.sql, puis de relancer l'installation ou le provisionning de la solution en fonction de la situation.
`vagrant up`
ou
`vagrant provision`